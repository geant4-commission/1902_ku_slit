//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// $Id: SlitDetectorConstruction.cc 101658 2016-11-21 09:00:41Z gcosmo $
//
/// \file SlitDetectorConstruction.cc
/// \brief Implementation of the SlitDetectorConstruction class

#include "SlitDetectorConstruction.hh"
#include "SlitDetectorMessenger.hh"

#include "G4Material.hh"
#include "G4NistManager.hh"
#include "G4SDManager.hh"

#include "G4AutoDelete.hh"
#include "G4Box.hh"
#include "G4GlobalMagFieldMessenger.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4Tubs.hh"

#include "G4GeometryManager.hh"
#include "G4GeometryTolerance.hh"

#include "G4UserLimits.hh"

#include "G4Colour.hh"
#include "G4VisAttributes.hh"

#include "G4SystemOfUnits.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4ThreadLocal G4GlobalMagFieldMessenger *SlitDetectorConstruction::fMagFieldMessenger = 0;

SlitDetectorConstruction::SlitDetectorConstruction()
    : G4VUserDetectorConstruction(), fTargetLV(nullptr), fTargetMaterial(nullptr),
      fChamberMaterial(nullptr), fWorldLength(5. * m), fSlitWidth(30 * um), fSlitThick(30 * um),
      fDetectionDistance(2. * m) {
    fMessenger = new SlitDetectorMessenger(this);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

SlitDetectorConstruction::~SlitDetectorConstruction() { delete fMessenger; }

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4VPhysicalVolume *SlitDetectorConstruction::Construct() {
    // Define materials
    DefineMaterials();

    // Define volumes
    return DefineVolumes();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void SlitDetectorConstruction::DefineMaterials() {
    // Material definition

    G4NistManager *nistManager = G4NistManager::Instance();

    // Lead defined using NIST Manager
    fTargetMaterial = nistManager->FindOrBuildMaterial("G4_Cu");

    // Xenon gas defined using NIST Manager
    fChamberMaterial = nistManager->FindOrBuildMaterial("G4_Galactic");

    // Print materials
    G4cout << *(G4Material::GetMaterialTable()) << G4endl;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4VPhysicalVolume *SlitDetectorConstruction::DefineVolumes() {
    // Sizes of the principal geometrical components (solids)

    G4double slitHalfSolidWidth = (fWorldLength - fSlitWidth) / 2.;
    G4ThreeVector slitPosition(0, fWorldLength / 2. - slitHalfSolidWidth / 2.);
    G4ThreeVector detectionPosition(0, 0, fDetectionDistance + gcPhantomThick / 2.);
    if (fDetectionDistance < 0.) detectionPosition *= -1.;

    // Definitions of Solids, Logical Volumes, Physical Volumes
    G4Box *slitHalfBox =
        new G4Box("SlitHalfBox", fWorldLength / 2., slitHalfSolidWidth / 2., fSlitThick / 2.);
    G4LogicalVolume *slitHalfLV = new G4LogicalVolume(slitHalfBox, fTargetMaterial, "SlitHalfLV");

    G4Box *detectorPhantomBox =
        new G4Box("DetectorPhantomBox", fWorldLength / 2., fWorldLength / 2., gcPhantomThick / 2.);
    fTargetLV = new G4LogicalVolume(detectorPhantomBox, fChamberMaterial, "DetectorPhantomLV");

    // World
    G4Box *worldS = new G4Box("world", fWorldLength / 2, fWorldLength / 2, fWorldLength / 2);
    G4LogicalVolume *worldLV = new G4LogicalVolume(worldS, fChamberMaterial, "World");

    //  Must place the World Physical volume unrotated at (0,0,0).
    //
    G4VPhysicalVolume *worldPV =
        new G4PVPlacement(0, G4ThreeVector(), worldLV, "World", 0, false, 0);

    new G4PVPlacement(nullptr, slitPosition, slitHalfLV, "SlitPV", worldLV, false, 0);
    new G4PVPlacement(nullptr, -slitPosition, slitHalfLV, "SlitPV", worldLV, false, 1);
    new G4PVPlacement(nullptr, detectionPosition, fTargetLV, "DetectorPhantomPV", worldLV, false,
                      0);

    // Always return the physical world
    return worldPV;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void SlitDetectorConstruction::ConstructSDandField() {}
