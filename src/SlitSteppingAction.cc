#include "SlitSteppingAction.hh"
#include "SlitDetectorConstruction.hh"

#include <iostream>

#include "G4LogicalVolumeStore.hh"
#include "G4MTRunManager.hh"
#include "G4Run.hh"
#include "G4Step.hh"
#include "G4SystemOfUnits.hh"

using namespace std;

inline bool is_approx_same(G4double a, G4double b) {
    static G4double tol = 1e-10;
    return fabs(a - b) < tol;
}

SlitSteppingAction::SlitSteppingAction(G4String aFileName) : fDefaultFileName(aFileName) {}

SlitSteppingAction::~SlitSteppingAction() { CloseFile(); }

void SlitSteppingAction::UserSteppingAction(const G4Step *aStep) {
    if (fOutfile.is_open()) {
        G4ThreeVector nowPosition                = aStep->GetTrack()->GetPosition();
        const SlitDetectorConstruction *fDetCons = static_cast<const SlitDetectorConstruction *>(
            G4MTRunManager::GetRunManager()->GetUserDetectorConstruction());
        if (aStep->IsFirstStepInVolume() &&
            is_approx_same(nowPosition.getZ(), fDetCons->GetDetectionDistance())) {
            fOutfile << nowPosition.getX() / mm << " " << nowPosition.getY() / mm << " "
                     << aStep->GetTrack()->GetDefinition()->GetParticleName() << " "
                     << aStep->GetTrack()->GetKineticEnergy() / MeV << endl;
        }
    }
}
