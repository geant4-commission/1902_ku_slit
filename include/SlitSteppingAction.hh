#ifndef __SlitSteppingAction_h
#define __SlitSteppinAction_h 1

#include <fstream>

#include "G4UserSteppingAction.hh"
#include "globals.hh"

class G4Step;

class SlitSteppingAction : public G4UserSteppingAction {
  public:
    SlitSteppingAction(G4String);
    virtual ~SlitSteppingAction();

    virtual void UserSteppingAction(const G4Step *) override;

    inline void OpenFile(G4String aName = "") {
        if (aName == "")
            fDefaultFileName = "output.txt";
        else
            fDefaultFileName = aName;
        CloseFile();
        fOutfile.open(fDefaultFileName);
    }

    inline void CloseFile() {
        if (fOutfile.is_open()) fOutfile.close();
    }

  private:
    G4String fDefaultFileName;
    std::ofstream fOutfile;
};

#endif
