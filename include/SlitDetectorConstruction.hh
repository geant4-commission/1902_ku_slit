//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// $Id: SlitDetectorConstruction.hh 73722 2013-09-09 10:23:05Z gcosmo $
//
/// \file SlitDetectorConstruction.hh
/// \brief Definition of the SlitDetectorConstruction class

#ifndef SlitDetectorConstruction_h
#define SlitDetectorConstruction_h 1

#include "G4SystemOfUnits.hh"
#include "G4VUserDetectorConstruction.hh"
#include "globals.hh"
#include "tls.hh"

class G4VPhysicalVolume;
class G4LogicalVolume;
class G4Material;
class G4UserLimits;
class G4GlobalMagFieldMessenger;

class SlitDetectorMessenger;

/// Detector construction class to define materials, geometry
/// and global uniform magnetic field.

class SlitDetectorConstruction : public G4VUserDetectorConstruction {
  public:
    SlitDetectorConstruction();
    virtual ~SlitDetectorConstruction();

  public:
    virtual G4VPhysicalVolume *Construct();
    virtual void ConstructSDandField();

    inline void SetSlitThick(G4double a) { fSlitThick = a; }
    inline void SetSlitWidth(G4double a) { fSlitWidth = a; }
    inline void SetDetectionDistance(G4double a) {
        if (a > fWorldLength / 2.) {
            fDetectionDistance = fWorldLength / 2. - gcPhantomThick;
        } else
            fDetectionDistance = a;
    }
    inline void SetWorldLength(G4double a) {
        if (a < fDetectionDistance * 2) {
            fWorldLength = fDetectionDistance * 2. + gcPhantomThick / 2.;
        } else
            fWorldLength = a;
    }

    inline G4double GetSlitThick() const { return fSlitThick; }
    inline G4double GetSlitWidth() const { return fSlitWidth; }
    inline G4double GetDetectionDistance() const { return fDetectionDistance; }
    inline G4double GetWorldLength() { return fWorldLength; }

    inline const G4LogicalVolume *GetTargetLV() const { return fTargetLV; }

  private:
    // methods
    void DefineMaterials();
    G4VPhysicalVolume *DefineVolumes();

    G4LogicalVolume *fTargetLV; // pointer to the logical Target

    G4Material *fTargetMaterial;  // pointer to the target  material
    G4Material *fChamberMaterial; // pointer to the chamber material

    SlitDetectorMessenger *fMessenger; // messenger

    G4double fWorldLength;
    G4double fSlitWidth;
    G4double fSlitThick;
    G4double fDetectionDistance;
    static constexpr G4double gcPhantomThick = 1 * um;

    static G4ThreadLocal G4GlobalMagFieldMessenger *fMagFieldMessenger;
    // magnetic field messenger
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif
